/* eslint-disable no-console */
import Processor from '../../index'

const processor = new Processor()

const maxCpuUsage = 10
const maxMemoryUsage = 1024
const statTimeout = 500

console.log('starting... (press CTRL+C to stop)')

processor.on('stat', () => process.stdout.write('.'))

processor.start(maxCpuUsage, maxMemoryUsage, statTimeout)
