import React, { Component } from 'react'
import { render } from 'react-dom'
import { ipcRenderer } from 'electron'

class App extends Component {
  constructor() {
    super()
    this.state = {
      started: false,
      data: [],
    }
  }

  componentDidMount() {
    ipcRenderer.on('processor-stat', () => this.setState({ data: this.state.data.concat([1]) }))
  }

  onClick = () => {
    const { started } = this.state

    if (started) {
      ipcRenderer.send('stop-processor')
      this.setState({ started: false })
    } else {
      ipcRenderer.send('start-processor')
      this.setState({ started: true })
    }
  }

  render() {
    const { started, data } = this.state

    return (
      <div>
        <div>
          <button onClick={this.onClick}>
            {started ? 'Stop' : 'Start'}
          </button>
        </div>
        {data.map((item, index) => (
          <div key={index}>
            Stat data from processor
          </div>
        ))}
      </div>
    )
  }
}

window.onload = () => render(<App />, document.body)
