import { app, BrowserWindow, ipcMain } from 'electron'
import Processor from '../../index'

let mainWindow
const processor = new Processor()

function createWindow() {
  mainWindow = new BrowserWindow({ width: 600, height: 500 })
  mainWindow.loadURL(`file://${__dirname}/index.html`)
  mainWindow.on('closed', () => { mainWindow = null })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    processor.stop()
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

let start = false

processor.on('stat', () => {
  if (mainWindow) {
    mainWindow.webContents.send('processor-stat')
  }
})

ipcMain.on('start-processor', () => { start = true })
ipcMain.on('stop-processor', () => { start = false })

setInterval(() => {
  if (start) {
    processor.start(10, 1024, 1000)
  } else {
    processor.stop()
  }
}, 1000)
