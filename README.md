Aversis Dll Repository
======================

## Requirements

* NodeJS (tested on version 6.3.0)
* Update npm to latest version `npm -g install npm@next`
* node-gyp [tools](https://github.com/nodejs/node-gyp#installation)

## Dependencies

```
npm install
```

## Commands

Build x64 Electron version

```
npm run build:electron:x64
```

Build ia32 Electron version

```
npm run build:electron:ia32
```

Build debug NodeJS version

```
npm run build:node
```

## Examples

Run NodeJS console example

```
npm run build:node && npm run examples:console
```

Run Electron demo app

```
npm run build:electron && npm run examples:electron
```
