class FakeProcessor {
  start(maxCpuUsage = 0, maxMemoryUsage = 0, repeat = 0) {
    setTimeout(() => {
      this.interval = setInterval(() => this.emit('stat'), repeat)
    }, 0)
  }

  stop() {
    if (this.interval) {
      clearInterval(this.interval)
    }
  }
}

export default FakeProcessor
