#include <node.h>
#include <iostream>
#include "processor.h"

Processor::Processor() {};
Processor::~Processor() {
  if (emit != nullptr) {
    delete emit;
  }
};

Nan::Persistent<v8::Function> Processor::constructor;
void Processor::Init(v8::Local<v8::Object> exports) {
  Nan::HandleScope scope;

  v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
  tpl->SetClassName(Nan::New("Processor").ToLocalChecked());
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  Nan::SetPrototypeMethod(tpl, "start", Start);
  Nan::SetPrototypeMethod(tpl, "stop", Stop);

  constructor.Reset(tpl->GetFunction());
  exports->Set(Nan::New("Processor").ToLocalChecked(), tpl->GetFunction());
}

void Processor::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
  if (!info.IsConstructCall()) {
    return Nan::ThrowError("`new` required");
  }
  Processor* obj = new Processor();
  obj->Wrap(info.This());

  obj->emit = new Nan::Callback(
    v8::Local<v8::Function>::Cast(obj->handle()->Get(Nan::New("emit").ToLocalChecked())));
  info.GetReturnValue().Set(info.This());
}

void Processor::Start(const Nan::FunctionCallbackInfo<v8::Value>& info) {
  Processor* obj = Nan::ObjectWrap::Unwrap<Processor>(info.Holder());
  if (obj->timer_req_ != nullptr) {
    return;
  }

  uint64_t max_cpu_usage = info[0]->IsUndefined() ? 0 : info[0]->NumberValue();
  uint64_t max_memory_usage = info[1]->IsUndefined() ? 1 : info[1]->NumberValue();
  uint64_t stat_timeout = info[2]->IsUndefined() ? 2 : info[2]->NumberValue();

  uv_timer_t *tr = new uv_timer_t;
  obj->timer_req_ = tr;
  tr->data = obj;
  uv_timer_init(uv_default_loop(), tr);
  uv_timer_start(tr, onStat, 0, stat_timeout);
}

void Processor::Stop(const Nan::FunctionCallbackInfo<v8::Value>& info) {
  Processor* obj = Nan::ObjectWrap::Unwrap<Processor>(info.Holder());
  if (obj->timer_req_ == nullptr) {
    return;
  }
  uv_timer_stop(obj->timer_req_);
  delete obj->timer_req_;
  obj->timer_req_ = nullptr;
}

void Processor::onStat(uv_timer_t *handle) {
  Nan::HandleScope scope;
  Processor *obj = static_cast<Processor*>(handle->data);
  v8::Local<v8::Value> argv[] = { Nan::New("stat").ToLocalChecked() };
  obj->emit->Call(obj->handle(), 1, argv);
}
