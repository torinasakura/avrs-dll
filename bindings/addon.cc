#include <nan.h>
#include "processor.h"

void InitAll(v8::Local<v8::Object> exports) {
  Processor::Init(exports);
}

NODE_MODULE(addon, InitAll)
