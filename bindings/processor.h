#ifndef PROCESSOR_H_
#define PROCESSOR_H_

#include <nan.h>
    
class Processor : public Nan::ObjectWrap {
 public:
  Nan::Callback* emit = nullptr;
  static void Init(v8::Local<v8::Object> exports);
  static void onStat(uv_timer_t *handle);
 private:
  Processor();
  ~Processor();

  static void New(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void Start(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static void Stop(const Nan::FunctionCallbackInfo<v8::Value>& info);
  static Nan::Persistent<v8::Function> constructor;
  uv_timer_t *timer_req_ = nullptr;
};


#endif  // PROCESSOR_H_
