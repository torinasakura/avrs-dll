{
  'variables': {
    'runtime%': 'node',
  },
  'targets': [
  ],
  'conditions': [
    ['OS=="win"', {
      'targets': [
        {
          'target_name': 'addon',
          'sources': [ 'bindings/addon.cc', 'bindings/processor.cc' ],
          'include_dirs': [
            '<!(node -e "require(\'nan\')")'
          ]
        },
        {
          'target_name': 'copy_binary',
          'type':'none',
          'dependencies' : [ 'addon' ],
          'copies': [
            {
             'destination': '<(module_root_dir)/dist/<(runtime)/<(target_arch)/',
             'files': ['<(module_root_dir)/build/Release/addon.node']
            }
          ]
        }
      ],
    }],
  ],
}
