/* eslint-disable global-require */
/* eslint-disable no-proto */
import { EventEmitter } from 'events'
import FakeProcessor from './fake'

const getProcessor = () => {
  const arch = process.arch
  const isWin = process.platform === 'win32'
  const isElectron = !!process.versions.electron

  if (isWin && isElectron) {
    return require(`./dist/electron/${arch}/addon.node`).Processor
  } else if (isWin) {
    return require(`./dist/node/${arch}/addon.node`).Processor
  }

  return FakeProcessor
}

const Processor = getProcessor()

Processor.prototype.__proto__ = EventEmitter.prototype

export default Processor
